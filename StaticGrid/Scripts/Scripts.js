﻿$(document).ready(function () {

    var GridManagerObj1 = new GridManager();
    GridManagerObj1.GridContainerObj = $('#GridContainer1');
    GridManagerObj1.Columns = Colums.slice();
    GridManagerObj1.Data = Employees;
    GridManagerObj1.ExtraData = ExtraData;
    GridManagerObj1.SearchTextBoxObj = $('#SearchTb1');
    GridManagerObj1.IsDeleteEnabled = false;
    GridManagerObj1.Initiate()

    var GridManagerObj2 = new GridManager();
    GridManagerObj2.IsDeleteEnabled = true;
    GridManagerObj2.GridContainerObj = $('#GridContainer2');
    GridManagerObj2.Columns = Colums.slice();
    GridManagerObj2.Data = Employees;
    GridManagerObj2.SearchTextBoxObj = $('#SearchTb2')
    GridManagerObj2.Initiate();


    //var Obj1 = new TestCls();
    //Obj1.Flag = false;
    //var Obj2 = new TestCls();
    //Obj2.Flag = true;



   
});
var TestCls = function () {
    this.Flag = false;
}
var Colums = [
    {
        name: 'FirstName',
        type: 'text',
        caption: 'First Name',
        itemtemplate: function (Row, Col) {
            if (Row[Col.name + 'Data'] != null)
                return '<a href="' + Row[Col.name + 'Data'] + '">' + Row[Col.name] + '</a>'
            else
                return '<span>' + Row[Col.name] + '</span>';
        }
    },
    { name: 'LastName', type: 'text', caption: 'Last Name' },
    { name: 'Age', type: 'number', caption: 'Age' },
    { name: 'Address', type: 'text', caption: 'Address' },
];

var Employees = [
    { FirstName: 'John', LastName: 'Shehata', Age: 34, Address: '507 - 20th Ave. E.Apt. 2A' },
    { FirstName: 'Andrew', LastName: 'Fuller', Age: 22, Address: '908 W. Capital Way' },
    { FirstName: 'Janet', LastName: 'Leverling', Age: 21, Address: '722 Moss Bay Blvd.' },
    { FirstName: 'Margaret', LastName: 'Peacock', Age: 36, Address: '4110 Old Redmond Rd.' },
    { FirstName: 'Steven', LastName: 'Buchanan', Age: 31, Address: '14 Garrett Hill' },
    { FirstName: 'Micho', LastName: 'Tiko', Age: null, Address: null },
];

var ExtraData = [
    { FirstName: null, LastName: 'http://www.google.com', Age: null, Address: 'http://www.google.com' },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: 'http://www.google.com' },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
    { FirstName: 'http://www.google.com', LastName: 'http://www.google.com', Age: null, Address: null },
];

