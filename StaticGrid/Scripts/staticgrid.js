﻿var GridManager = function () {

    var MyCls = this;
    var TheadObj = $('<thead></thead>');
    var TbodyObj = $('<tbody></tbody>');
    var MainTblObj = $('<table class="table table-striped table-hover beegrid"></table>');
    var RefreshGrid = function () {
        Sorting();
        BindGridBody();
        ChangeSortHeader();

    };
    var BindGridBody = function () {
        var term = $(MyCls.SearchTextBoxObj).val();
        $(TbodyObj).html('');
        MyCls.Data.filter(function (d) { return SearchGrid(d, term) }).forEach(function (RowElem) {
            TrObj = $('<tr></tr>')
            MyCls.Columns.forEach(function (ColElem) {
                if (ColElem.type == 'control') {
                    if (ColElem.name == 'delete') {
                        var DeleteObj = $('<span class="glyphicon glyphicon-trash"></span>').click(function () {
                            MyCls.DeleteCallBack(RowElem);
                        });
                        TdObj = $('<td></td>');
                        $(TdObj).append(DeleteObj)
                    }
                }
                else {
                    if (ColElem.itemtemplate != null)
                        TdObj = $('<td>' + ColElem.itemtemplate(RowElem, ColElem) + '</td>');
                    else
                        TdObj = $('<td>' + MyCls.DefaultRender(RowElem, ColElem) + '</td>');
                }



                $(TrObj).append(TdObj);
            })
            $(TbodyObj).append(TrObj);
        })
    };
    var BindGridHeader = function () {
        TrObj = $('<tr></tr>')
        MyCls.Columns.forEach(function (Elem) {
            if (Elem.type != 'control')
                ThObj = $('<th data-name="' + Elem.name + '"><span></span><span>' + Elem.caption + '</span></th>');
            else
                ThObj = $('<th data-type="' + Elem.type + '"></th>');

            $(TrObj).append(ThObj);
        });
        $(TheadObj).append(TrObj);
        ChangeSortHeader();
    };
    var CheckNumber = function (value) {
        return (isNaN(value) ? 0 : Number(value));
    };
    var Sorting = function () {
        MyCls.Data.sort(function (a, b) {
            var Obj1, Obj2;
            if (MyCls.Columns.find(function (C) { return C.name == MyCls.SortColumn }).type == 'text') {
                Obj1 = (a[MyCls.SortColumn] != null ? a[MyCls.SortColumn].toLowerCase() : '')
                Obj2 = (b[MyCls.SortColumn] != null ? b[MyCls.SortColumn].toLowerCase() : '')
            }
            else {
                Obj1 = CheckNumber(a[MyCls.SortColumn]);
                Obj2 = CheckNumber(b[MyCls.SortColumn]);
            }
            if (Obj1 > Obj2)
                return (MyCls.SortDirection == 'asc' ? 1 : -1);
            else if (Obj1 < Obj2)
                return (MyCls.SortDirection == 'asc' ? -1 : 1);
            else
                return 0;
        });
    }
    var ChangeSortHeader = function () {
        $(TheadObj).find('th').each(function () {
            var SpanObj = $(this).find('span:first-child');
            $(SpanObj).removeClass('glyphicon');
            $(SpanObj).removeClass('glyphicon-sort-by-attributes');
            $(SpanObj).removeClass('glyphicon-sort-by-attributes-alt');

            if ($(this).data('name') == MyCls.SortColumn) {
                if (MyCls.SortDirection == 'asc')
                    $(SpanObj).addClass('glyphicon glyphicon-sort-by-attributes')
                else
                    $(SpanObj).addClass('glyphicon glyphicon-sort-by-attributes-alt')

            }
        });
    };
    var SearchGrid = function (Elem, term) {
        var Flag = false;
        if (term == null || term.trim() == '') return true;
        MyCls.Columns.filter(function (d) { return d.type != 'control' }).forEach(function (col) {
            if (Elem[col.name] != null && Elem[col.name].toString().toLowerCase().indexOf(term.toLowerCase()) > -1)
                if (!Flag) Flag = true;
        });
        return Flag;
    }
    var MergeExtraData = function () {
        if (MyCls.ExtraData != null && MyCls.ExtraData.length > 0) {
            for (var i = 0; i < MyCls.Data.length; i++) {
                MyCls.Columns.forEach(function (Col) {
                    if (MyCls.ExtraData[i] != null && MyCls.ExtraData[i][Col.name] != null && MyCls.ExtraData[i][Col.name].toString().trim() != '') {
                        MyCls.Data[i][Col.name + 'Data'] = MyCls.ExtraData[i][Col.name];
                    }
                    else {
                        MyCls.Data[i][Col.name + 'Data'] = null;
                    }
                });
            }
        }
    }

    this.SearchTextBoxObj = null;
    this.SortColumn = null;
    this.SortDirection = null;
    this.Data = null,
    this.ExtraData = null;
    this.Columns = null,
    this.GridContainerObj = null,
    this.DefaultRender = function (RowElem, ColElem) {
        return '<span>' + RowElem[ColElem.name] + '</span>'
    },
    this.DeleteCallBack = function (RowObj) {
    }
    this.IsDeleteEnabled = false;
    this.Initiate = function () {

        var TrObj = null;;
        var ThObj = null;
        var TdObj = null;

        if (MyCls.IsDeleteEnabled == true)
            MyCls.Columns.push({
                name: 'delete',
                type: 'control',
                itemtemplate: function (Row, Col) {
                    return '<span class="glyphicon glyphicon-trash"></span>'
                }
            });


        if (this.SortColumn == null)
            this.SortColumn = this.Columns[0].name;

        if (this.SortDirection == null)
            this.SortDirection = 'asc';

        MergeExtraData();
        Sorting();
        BindGridHeader();
        BindGridBody();

        $(MainTblObj).append(TheadObj).append(TbodyObj);
        $(this.GridContainerObj).html('');
        $(this.GridContainerObj).append($(MainTblObj));

        $(this.GridContainerObj).find('th').not('[data-type="control"]').click(function () {
            var NewSortColumn = $(this).data('name');
            if (NewSortColumn == null) return;
            if (MyCls.SortColumn == NewSortColumn) {
                (MyCls.SortDirection == 'asc' ? MyCls.SortDirection = 'desc' : MyCls.SortDirection = 'asc')
            }
            else {
                MyCls.SortColumn = NewSortColumn;
                MyCls.SortDirection = 'asc'
            }
            RefreshGrid();
        });

        $(this.SearchTextBoxObj).keyup(function () {
            RefreshGrid();
        })

    }


}